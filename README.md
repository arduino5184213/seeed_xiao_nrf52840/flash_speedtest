# QSPI Flash Example

## Why this example?

Because I struggled for hours finding out how to use the QSPI flash memory
provided on the *XIAO nrf52840* from __Seeed Stutio__ ®.

I guess that other people will face the same issue with other boards from
other manufacturers.

## Special thanks

__Adafruit__ ® and __Arduino__ ® do a really great job by providing awesome
hardwares and libraries.


This example mainly relies on the [SPIFlash](https://github.com/adafruit/Adafruit_SPIFlash)
library from __Adafruit__.

## About that example

This example will write data to the QSPI Flash and read it back for check. It
is adapted from the example in the __SPIFlash__ library. One may think that
there is no much difference between _flash_speedtest.ino_ and the original
one. That's true, but I wish I had this file before starting.

## Adapting this to another board

1. you need a processor supported by the __SPIFlash__ library.

2. you need to adapt the QSPI pins if needed

3. If the `SPIFlash_Device_t` is not defined in _flash_devices.h_ define your
own as it is done here.
